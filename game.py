from random import randint  #random is the module and randint is the method

#Generates a list of four, unique, single-digit numbers

def generate_secret():                                                             # 1st task
    my_list = []

    while len(my_list) < 4:
        n = randint(0, 9)

        if n not in my_list:
            my_list.append(n)    
    return my_list


def parse_numbers(s):                                                              # 2nd task
    l = []
    for n in s:
        number = int(n)
        l.append(number)
    return l



def count_exact_matches(first, second):                                             # 3rd task
    count = 0
    for a, b in zip(first, second):
        if a == b:
            count = count + 1
    return count


def count_common_enteries(first, second):                                               #4th task
    count = 0
    for entry in first:
        if entry in second:
            count = count + 1
    return count





def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    number_of_guesses = 20
    print('secret: ', secret)

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)



        # TODO:
        # while the length of their response is not equal                          1
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again

        while len(guess) != 4:      # Use guess because the input is guess
            print('you must enter a four digit number')
            guess = input(prompt)



                # TODO:
                # write a function and call it here to:                            2
                # convert the string in the variable guess to a list
                # of four single-digit numbers

        converted_guess = parse_numbers(guess)

                # TODO:
                # write a function and call it here to:                             3
                # count the number of exact matches between the
                # converted guess and the secret

        total_exact_matches = count_exact_matches(converted_guess, secret)
        total_common_enteries = count_common_enteries(converted_guess, secret)

        print("total_exact_matches: ", total_exact_matches)
        if total_exact_matches == 4:
            print("You guessed the numbers")
            return 



                # TODO:
                # write a function and call it here to:                             4
                # count the number of common entries between the
                # converted guess and the secret
        


                # TODO:
                # if the number of exact matches is four, then                        5
                # the player has correctly guessed! tell them
                # so and return from the function.

                # TODO:
                # report the number of "bulls" (exact matches)
        print("total number of bulls: ", total_exact_matches)
                # TODO:
                # report the number of "cows" (common entries - exact matches)
        print("total number of cows: ", total_common_enteries - total_exact_matches)
                # TODO:
                # If they don't guess it in 20 tries, tell
                # them what the secret was.
        print("The secret was: ", secret) 

                # Runs the game
def run():
        response = input("Do you want to play a game?")
        
                # TODO: Delete the word pass, below, and have this function:
                # Call the play_game function
                # Ask if they want to play again
                # While the person wants to play the game
                #   Call the play_game function
                #   Ask if they want to play again
                #   pass

        while response == 'yes':
            play_game()
            response = input('Do you want to play a game?')

if __name__ == "__main__":
    run()